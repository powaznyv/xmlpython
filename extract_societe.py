import sys
from xml.dom.minidom import parse, parseString

# A partir d'un fichier xml, permet d'afficher les GU enregistré dans le dictionnaire des exploitants

#mapping societe <=> GU
exploitants = {
    "EDF_HYDROMED_GEH-DURANCE_GU-SERRE-PONCON": "GU-SERRE-PONCON"
}

#print(sys.argv[1])

xmldoc = parse(sys.argv[1])
matchingNodes = [node.childNodes[0].nodeValue  for node in xmldoc.getElementsByTagName("gu32:societe")]

#print(matchingNodes)

for exploitant in exploitants:
    if (exploitant in matchingNodes) :
        print (exploitants[exploitant])